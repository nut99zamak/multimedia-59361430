import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { PjHomePage } from './pj-home';

@NgModule({
  declarations: [
    PjHomePage,
  ],
  imports: [
    IonicPageModule.forChild(PjHomePage),
  ],
})
export class PjHomePageModule {}
