import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';

import { PopoverController } from 'ionic-angular';
import { PopoverHomePage } from '../popover-home/popover-home';
import { ManagePPage } from '../manage-p/manage-p';
/**
 * Generated class for the PjHomePage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-pj-home',
  templateUrl: 'pj-home.html',
  
})
export class PjHomePage {
  params: Object;
  pushPage: any;
  constructor(public navCtrl: NavController, public navParams: NavParams, 
    public popoverCtrl: PopoverController) {
      this.pushPage = ManagePPage;
      this.params = { id: 42 };
  }
 

  presentPopover(myEvent) {
    let popover = this.popoverCtrl.create(PopoverHomePage);
    popover.present({
      ev: myEvent
    });
  }


  

}


