int  ledPin = 8;                                //ledPin = Digital Pin-9
int  Volume = 0;                                //Volume = Analog Pin-0

void setup()                                    //Setup Function
{
  pinMode(ledPin, OUTPUT);                      //ledPin = Output
}

void loop()                                     //Main Function
{ 
  int val = analogRead(Volume);
  val = map(val, 0, 1023, 0, 255);              //Convert 0..1023 to 0..255
  analogWrite(ledPin,val);                      //Dimmer LED = 0..255 Scale
}