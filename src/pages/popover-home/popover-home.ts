import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, ViewController } from 'ionic-angular';


/**
 * Generated class for the PopoverHomePage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-popover-home',
  templateUrl: 'popover-home.html',
  template: `

  <div>
  <h1 style="padding-left: 2vw; padding-right: 2vw">Description</h1>
  <p style="padding-left: 2vw; padding-right: 2vw">This appplication is IoT Farm Manager 
  แสดงความเหมาะสมและความเสถียรภาพของฟาร์มที่ได้รับจากเซ็นเซอร์และนำมา
  คำนวณโดยแสดงผลผ่าน application</p>
  </div>
  `
})
export class PopoverHomePage {

  // constructor(public navCtrl: NavController, public navParams: NavParams) {
  // }
  constructor(public viewCtrl: ViewController) {}


  ionViewDidLoad() {
    console.log('ionViewDidLoad PopoverHomePage');
  }
  close() {
    this.viewCtrl.dismiss();
  }

}
